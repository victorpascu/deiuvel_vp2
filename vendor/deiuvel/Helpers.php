<?php

function dd()
{
	die(var_dump(func_get_args()));
}

function get_prefix_length()
{
	return strpos($_SERVER['SCRIPT_NAME'], 'index.php') - 1;
}

function get_current_route()
{
	return substr($_SERVER['REQUEST_URI'], get_prefix_length());
}

function get_prefix_path()
{
	return substr($_SERVER['SCRIPT_NAME'], 0, get_prefix_length());
}

function get_path($key)
{
	global $paths;

	return $paths[$key];
}