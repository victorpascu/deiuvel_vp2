<?php

class Route
{
	protected static $routes = [];

	public static function get($uri, $output)
	{
		static::$routes[$uri] = $output;
	}

	public static function getRoute($uri)
	{
		return static::routeExists($uri) ? static::$routes[$uri] : App::getError();
	}

	public static function routeExists($uri)
	{
		return isset(static::$routes[$uri]);
	}
}